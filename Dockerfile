FROM registry.gitlab.com/grahamcampbell/php:7.4-base

ENV XDEBUG_VERSION 3.1.0

RUN pecl install xdebug-$XDEBUG_VERSION \
    && docker-php-ext-enable xdebug
